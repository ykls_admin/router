package com.icss.newretail.config.netty;

import java.util.concurrent.TimeUnit;

import org.springframework.scheduling.annotation.Async;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoop;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.CharsetUtil;
import lombok.extern.slf4j.Slf4j;

/**
 *
 */
@Slf4j
public class NettyClient {
  private String routerAdminIp;
  private Integer routerAdminPort;
  private EventLoopGroup group;
  private Bootstrap bootstrap;

  public NettyClient(String routerAdminIp, Integer routerAdminPort){
    this.routerAdminIp = routerAdminIp;
    this.routerAdminPort = routerAdminPort;
    this.group = new NioEventLoopGroup();
    NettyClient that = this;
    this.bootstrap = new Bootstrap()
        .group(group)
        //禁止使用Nagle算法，即时传输，输入一次发送一次
        .option(ChannelOption.TCP_NODELAY, true)
        .channel(NioSocketChannel.class)
        .handler(new ChannelInitializer<SocketChannel>() {
          @Override
          protected void initChannel(SocketChannel socketChannel) throws Exception {
            ChannelPipeline pipeline = socketChannel.pipeline();
            pipeline.addLast("decoder", new StringDecoder(CharsetUtil.UTF_8));
            pipeline.addLast("encoder", new StringEncoder(CharsetUtil.UTF_8));
            //第一个参数 60 表示读操作空闲时间
            //第二个参数 20 表示写操作空闲时间
            //第三个参数 60*10 表示读写操作空闲时间
            pipeline.addLast("ping", new IdleStateHandler(10, 5, 1 * 10, TimeUnit.SECONDS));
            pipeline.addLast(new NettyClientHandler(that));
          }
        });
  }

  @Async
  public void start() {
    //连接不上无限重连
    try {
      ChannelFuture future = bootstrap.connect(routerAdminIp, routerAdminPort);
      future.addListener((ChannelFutureListener) future1 -> {
        if (!future1.isSuccess()) {
          final EventLoop loop = future1.channel().eventLoop();
          loop.schedule(() -> {
            log.info("router-admin连接不上...：开始重连");
            start();
          }, 10L, TimeUnit.SECONDS);
        } else {
          log.info("router-admin连接成功...：{}:{}", routerAdminIp, routerAdminPort);
        }
      });
    } catch (Exception e) {
      log.error("router客户端启动异常...", e);
    }
    //连接不上就优雅停机
//    try {
//      ChannelFuture future = bootstrap.connect(routerAdminIp, routerAdminPort).sync();
//      log.info("router-admin连接成功...：{}:{}", routerAdminIp, routerAdminPort);
//      // 阻塞线程，channel关闭后走finally
//      future.channel().closeFuture().sync();
//    } catch (Exception e) {
//      log.error("netty client init error.................", e);
//    } finally {
//      group.shutdownGracefully();
//    }
  }
}