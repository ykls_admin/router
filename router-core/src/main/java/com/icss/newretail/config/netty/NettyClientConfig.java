package com.icss.newretail.config.netty;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Configuration
@ConditionalOnMissingClass(value = "com.icss.newretail.config.netty.NettyServerConfig")
@ConditionalOnProperty(value = "router.admin.enable")
@Slf4j
@Data
public class NettyClientConfig {
  @Value("${router.admin.ip}")
  private String routerAdminIp;
  @Value("${router.admin.socketPort}")
  private Integer routerAdminPort;
  @Autowired
  private NettyClient nettyClient;
  @Bean
  public NettyClient nettyClient(){
    return new NettyClient(routerAdminIp, routerAdminPort);
  }
  @PostConstruct
  public void init(){
    nettyClient.start();
  }
}
