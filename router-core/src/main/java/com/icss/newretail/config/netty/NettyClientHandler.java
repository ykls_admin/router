package com.icss.newretail.config.netty;

import org.springframework.util.StringUtils;

import com.icss.newretail.router.RouterFactory;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import lombok.extern.slf4j.Slf4j;

/**
 * 客户端处理器
 */
@Slf4j
public class NettyClientHandler extends ChannelInboundHandlerAdapter {
  private NettyClient nettyClient;
  public NettyClientHandler(NettyClient nettyClient){
    this.nettyClient = nettyClient;
  }
  /**
   * 建立连接时会触发
   */
  @Override
  public void channelActive(ChannelHandlerContext ctx) throws Exception {
    log.info("client channel active......");
  }

  /**
   * 接受消息时会触发
   */
  @Override
  public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
    if(msg != null && !StringUtils.isEmpty(msg.toString())){
      if(msg.toString().startsWith("routerKey:")) {
        String routerKey = msg.toString().replace("routerKey:", "");
        RouterFactory.destroy(routerKey);
      }
    }
    log.info("client channel read......: {}", msg.toString());
  }

  /**
   * 发生异常触发
   */
  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
    log.error("client exception......", cause);
    ctx.close();
  }

  /**
   * 空闲事件监听
   */
  @Override
  public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
    super.userEventTriggered(ctx, evt);
    if (evt instanceof IdleStateEvent) {
      IdleStateEvent event = (IdleStateEvent) evt;
      //空闲N秒就发送心跳
      if (event.state().equals(IdleState.WRITER_IDLE)) {
        ctx.writeAndFlush("heat");
      }
    }
  }

  /**
   * channel不可用时触发
   */
  @Override
  public void channelInactive(ChannelHandlerContext ctx) throws Exception {
    super.channelInactive(ctx);
    log.info("client channel不可用，掉线重连");
    nettyClient.start();
//    final EventLoop eventLoop = ctx.channel().eventLoop();
//    eventLoop.schedule(() -> {
//
//    }, 5L, TimeUnit.SECONDS);
  }
}