package com.icss.newretail.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 路由方案表
 * </p>
 *
 * @author 
 * @since 2020-06-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class RouterVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String routerId;

    private String routerCode;

    private String routerName;

    private String handleType;

    private Integer status;

    private LocalDateTime createTime;

    private String createUser;

    private LocalDateTime updateTime;

    private String updateUser;

    private List<RouterRuleVo> routerRuleList;
}
