package com.icss.newretail.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 路由规则表
 * </p>
 *
 * @author 
 * @since 2020-06-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class RouterRuleVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String ruleId;

    private String routerId;

    private String ruleName;

    private String className;

    private String methodName;

    private Integer async;

    private Integer sortNum;

    private Integer status;

    private LocalDateTime createTime;

    private String createUser;

    private LocalDateTime updateTime;

    private String updateUser;
}
