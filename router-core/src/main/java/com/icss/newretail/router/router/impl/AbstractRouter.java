package com.icss.newretail.router.router.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.icss.newretail.router.checker.RouterDuplicateChecker;
import com.icss.newretail.router.checker.impl.RouterDuplicateCheckerInMemory;
import com.icss.newretail.router.exception.handler.RouterExceptionHandler;
import com.icss.newretail.router.exception.handler.impl.RouterExceptionHandlerOfMq;
import com.icss.newretail.router.router.Router;
import com.icss.newretail.router.rule.RouterRule;
import com.icss.newretail.router.rule.impl.AbstractRouterRule;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class AbstractRouter implements Router {
  //规则列表
  protected final List<RouterRule> routerRuleList = new ArrayList<>();

  //重复检查器
  protected RouterDuplicateChecker routerDuplicateChecker;

  //异常处理
  protected RouterExceptionHandler routerExceptionHandler;

  /**
   * 私有化无参构造函数
   */
  public AbstractRouter() {
    this.routerDuplicateChecker = new RouterDuplicateCheckerInMemory();
    this.routerExceptionHandler = new RouterExceptionHandlerOfMq();
  }

  /**
   * 构造函数
   */
  public AbstractRouter(RouterDuplicateChecker routerDuplicateChecker) {
    this.routerDuplicateChecker = routerDuplicateChecker;
    this.routerExceptionHandler = new RouterExceptionHandlerOfMq();
  }

  /**
   * 构造函数
   */
  public AbstractRouter(RouterExceptionHandler routerExceptionHandler) {
    this.routerDuplicateChecker = new RouterDuplicateCheckerInMemory();
    this.routerExceptionHandler = routerExceptionHandler;
  }

  /**
   * 开始一个新的规则
   */
  @Override
  public RouterRule async(boolean async) {
    return rule().setAsync(async);
  }

  /**
   * 开始一个新的规则
   */
  @Override
  public RouterRule rule() {
    RouterRule routerRule = new AbstractRouterRule(this);
    this.routerRuleList.add(routerRule);
    return routerRule;
  }

  /**
   * 获取规则列表
   */
  @Override
  public List<RouterRule> getRouterRuleList() {
    return routerRuleList;
  }

  /**
   * 执行方法调用链
   */
  @Override
  public void execute(Map<String, Object> context) {
    if (routerDuplicateChecker.isDuplicate(context)) {
      // 如果是重复消息，那么就不做处理
      return;
    }
    for (final RouterRule routerRule : routerRuleList) {
      routerRule.execute(context);
    }
  }

  /**
   * 销毁
   */
  @Override
  public void destroy() {
    for (final RouterRule routerRule : routerRuleList) {
      routerRule.destroy();
    }
  }
}
