package com.icss.newretail.router.checker;

import java.util.Map;

/**
 * 重复检查器
 */
public interface RouterDuplicateChecker {

  /**
   * 判断是否重复提交
   */
  boolean isDuplicate(Map<String, Object> context);
}
