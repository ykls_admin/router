package com.icss.newretail.router.rule;

import java.lang.reflect.Method;
import java.util.Map;

import com.icss.newretail.router.handler.RouterHandler;
import com.icss.newretail.router.router.Router;

public interface RouterRule {
  /**
   * 是否异步执行-get
   */
  boolean isAsync();
  /**
   * 是否异步执行-set
   */
  RouterRule setAsync(boolean async);
  /**
   * 添加处理类
   */
  Router addHandler(RouterHandler... handlers);
  /**
   * 添加处理类
   */
  Router addMethod(Object delegate, Method method);
  /**
   * 添加处理类
   */
  Router addMethod(Object delegate, String methodName);
  /**
   * 执行处理类
   */
  void execute(Map<String, Object> context);
  /**
   * 销毁
   */
  void destroy();
}
