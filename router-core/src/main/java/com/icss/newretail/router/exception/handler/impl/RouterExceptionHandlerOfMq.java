package com.icss.newretail.router.exception.handler.impl;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.amqp.core.Message;

import com.icss.newretail.router.exception.handler.RouterExceptionHandler;
import com.rabbitmq.client.Channel;

import lombok.extern.slf4j.Slf4j;

/**
 * 默认RouterErrorException异常处理器
 */
@Slf4j
public class RouterExceptionHandlerOfMq implements RouterExceptionHandler {
  @Override
  public void handle(Exception e, Message message, Channel channel, Map<String, Object> paramMap) {
    log.error("RouterExceptionHandlerOfMq handler执行出错: ", e);
    //异常入队列
    new Timer().schedule(new TimerTask() {
      public void run() {
        try {
          channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, true);
        } catch (Exception e1) {
          e1.printStackTrace();
        }
      }
    }, 1000);
  }
}
