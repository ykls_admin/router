package com.icss.newretail.router.rule.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.CollectionUtils;

import com.icss.newretail.router.handler.RouterHandler;
import com.icss.newretail.router.router.Router;
import com.icss.newretail.router.rule.RouterRule;

import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

/**
 * 路由器规则
 */
@Data
@Slf4j
@Accessors(chain = true)
public class AbstractRouterRule implements RouterRule {
  //路由器
  protected final Router router;
  //同步还是异步
  protected boolean async = true;
  //处理类
  protected List<RouterHandler> handlers = new ArrayList<>();
  //处理类
  protected Object delegate;
  protected List<Method> methods = new ArrayList<>();
  /**
   * 构造函数
   */
  public AbstractRouterRule(Router router) {
    this.router = router;
  }

  /**
   * 添加处理类
   */
  @Override
  public Router addHandler(RouterHandler... handlers) {
    if (handlers != null && handlers.length > 0) {
      for (RouterHandler routerHandler : handlers) {
        this.handlers.add(routerHandler);
      }
    }
    return router;
  }

  /**
   * 添加处理类
   */
  @Override
  public Router addMethod(Object delegate, Method method) {
    this.delegate = delegate;
    if (method != null) {
      this.methods.add(method);
    }
    return router;
  }

  /**
   * 添加处理类
   */
  @Override
  public Router addMethod(Object delegate, String methodName) {
    this.delegate = delegate;
    try {
      Method method = delegate.getClass().getMethod(methodName, Map.class);
      if (method != null) {
        this.methods.add(method);
      }
    } catch (Exception e) {
      log.error("找不到方法"+methodName, e);
      throw new RuntimeException("创建router失败".concat(e.getMessage()));
    }
    return router;
  }

  /**
   * 执行规则内的处理类
   */
  @Override
  public void execute(Map<String, Object> context) {
    if (context == null) {
      context = new HashMap<>();
    }
    if(CollectionUtils.isEmpty(handlers) && CollectionUtils.isEmpty(methods)){
      throw new RuntimeException("清配置处理类");
    }
    //同步执行handler
    if (!async) {
      if (!CollectionUtils.isEmpty(handlers)) {
        for (RouterHandler handler : this.handlers) {
          handler.handle(context);
        }
      }
      if (!CollectionUtils.isEmpty(methods)) {
        //同步执行handler
        for (Method method : methods) {
          try {
            method.invoke(delegate, context);
          } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
          } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
          }
        }
      }
    }
  }
  /**
   * 销毁
   */
  @Override
  public void destroy(){
//    handlers = null;
//    delegate = null;
//    methods = null;
  }
}
