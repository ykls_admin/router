package com.icss.newretail.router.exception.handler;

import java.util.Map;

import org.springframework.amqp.core.Message;

import com.rabbitmq.client.Channel;

/**
 * RouterErrorException异常处理器
 */
public interface RouterExceptionHandler {
  void handle(Exception e, Message message, Channel channel, Map<String, Object> paramMap);
}
