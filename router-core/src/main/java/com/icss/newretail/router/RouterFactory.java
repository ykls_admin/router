package com.icss.newretail.router;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSONObject;
import com.icss.newretail.model.RouterRuleVo;
import com.icss.newretail.model.RouterVo;
import com.icss.newretail.router.exception.handler.impl.RouterExceptionHandlerOfAdmin;
import com.icss.newretail.router.router.Router;
import com.icss.newretail.router.router.impl.RouterWithRabbitMq;

import lombok.extern.slf4j.Slf4j;

/**
 * router工厂，根据key创建并返回单例模式的router
 */
@Component
@Slf4j
public class RouterFactory {
  @Autowired
  private RabbitAdmin rabbitAdmin;
  @Value("${router.admin.ip:''}")
  private String routerAdminIp;
  @Value("${router.admin.webPort:8080}")
  private Integer webPort;
  @Autowired
  private ApplicationContext applicationContext;
  @Autowired
  private RestTemplate restTemplateOfRouter;
  public static Map<String, Router> routerMap = new ConcurrentHashMap<>();
  /**
   * 单体应用模式构建规则
   * @param routerKey
   * @return
   */
  public Router router(String routerKey, RouterFactoryCallback routerFactoryCallback){
    Router router = routerMap.get(routerKey);
    if(router != null){
      return router;
    }
    synchronized ("routerKey:".concat(routerKey).intern()) {
      router = routerMap.get(routerKey);
      if(router != null){
        return router;
      }
      router = new RouterWithRabbitMq(rabbitAdmin);
      routerFactoryCallback.operation(router);
      routerMap.put(routerKey, router);
    }
    return router;
  }

  /**
   * 从注册中心拿规则创建单例router
   * @param routerKey
   * @return
   */
  public Router routerFromAdmin(String routerKey){
    if(routerAdminIp == null || StringUtils.isEmpty(routerAdminIp)){
      throw new RuntimeException("配置文件未配置router-admin");
    }
    Router router = routerMap.get(routerKey);
    if(router != null){
      return router;
    }
    synchronized ("routerKey:".concat(routerKey).intern()) {
      router = routerMap.get(routerKey);
      if(router != null){
        return router;
      }

      //从注册中心拿规则
//      RouterVo routerVo = restTemplateOfRouter.getForObject("http://"+routerAdminIp+":"+webPort+"/router-admin/router/getRouterByCode?routerCode=" + routerKey, RouterVo.class);
      String result = restTemplateOfRouter.getForObject("http://"+routerAdminIp+":"+webPort+"/router-admin/router/getRouterByCode?routerCode=" + routerKey, String.class);
      if (StringUtils.isEmpty(result)) {
        throw new RuntimeException("未查询到路由规则，请检查 "+routerKey+" 配置");
      }
      RouterVo routerVo = JSONObject.parseObject(result, RouterVo.class);
      if (routerVo == null || CollectionUtils.isEmpty(routerVo.getRouterRuleList())) {
        throw new RuntimeException("未查询到路由规则，请检查 "+routerKey+" 配置");
      }
      //设置异常处理方式
      if (!StringUtils.isEmpty(routerVo.getHandleType()) && "ADMIN".equals(routerVo.getHandleType())) {
        router = new RouterWithRabbitMq(new RouterExceptionHandlerOfAdmin(restTemplateOfRouter, routerAdminIp, webPort), rabbitAdmin);
      } else {
        router = new RouterWithRabbitMq(rabbitAdmin);
      }
//      RouterRuleVo[] routerRuleVos = restTemplateOfRouter.getForObject("http://"+routerAdminIp+":"+webPort+"/routerRule/getRouterRuleListByRouterCode?routerCode=" + routerKey, RouterRuleVo[].class);
      for (RouterRuleVo routerRuleVo : routerVo.getRouterRuleList()) {
        Object o = applicationContext.getBean(routerRuleVo.getClassName());
        try {
          Method handleMethod = o.getClass().getMethod(routerRuleVo.getMethodName(), Map.class);
          router.async(routerRuleVo.getAsync() == 1 ? true : false).addMethod(o, handleMethod);
        } catch (Exception e) {
          log.error("创建router失败", e);
          throw new RuntimeException("创建router失败".concat(e.getMessage()));
        }
      }
      routerMap.put(routerKey, router);
    }
    return router;
  }
  /**
   * 销毁router
   * @param routerKey
   * @return
   */
  public static void destroy(String routerKey){
    if(!StringUtils.isEmpty(routerKey) && routerMap.containsKey(routerKey)){
      routerMap.get(routerKey).destroy();
      routerMap.remove(routerKey);
    }
  }
}
