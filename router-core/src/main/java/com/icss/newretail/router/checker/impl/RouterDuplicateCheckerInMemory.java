package com.icss.newretail.router.checker.impl;

import java.util.Map;

import com.icss.newretail.router.checker.RouterDuplicateChecker;

/**
 * 默认重复提交检查器
 */
public class RouterDuplicateCheckerInMemory implements RouterDuplicateChecker {

  /**
   * 过期时间：15秒.
   */
  private final Long timeToLive;

  /**
   * 无参构造方法
   */
  public RouterDuplicateCheckerInMemory() {
    this.timeToLive = 15 * 1000L;
  }

  /**
   * 构造方法.
   *
   * @param timeToLive  一个消息ID在内存的过期时间：毫秒
   */
  public RouterDuplicateCheckerInMemory(Long timeToLive) {
    this.timeToLive = timeToLive;
  }

  @Override
  public boolean isDuplicate(Map<String, Object> context) {
    return false;
  }
}
