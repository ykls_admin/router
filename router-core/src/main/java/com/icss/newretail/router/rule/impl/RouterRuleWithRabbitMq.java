package com.icss.newretail.router.rule.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.listener.MessageListenerContainer;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.util.CollectionUtils;

import com.icss.newretail.router.handler.RouterHandler;
import com.icss.newretail.router.router.Router;
import com.icss.newretail.router.router.impl.RouterWithRabbitMq;

import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

/**
 * 路由器规则
 */
@Slf4j
@Accessors(chain = true)
public class RouterRuleWithRabbitMq extends AbstractRouterRule {
  //消息监听容器列表
  private List<MessageListenerContainer> messageListenerContainers = new ArrayList<>();
  /**
   * 构造函数
   * @param router
   */
  public RouterRuleWithRabbitMq(RouterWithRabbitMq router) {
    super(router);
  }

  /**
   * 添加处理器
   */
  @Override
  public Router addHandler(RouterHandler... handlers) {
    super.addHandler(handlers);
    RouterWithRabbitMq routerWithRabbitMq = (RouterWithRabbitMq) router;
    RabbitAdmin rabbitAdmin = routerWithRabbitMq.getRabbitAdmin();
    //注册队列和binding
    if (async) {
      for (RouterHandler routerHandler : handlers) {
        String queueName = routerHandler.getClass().getName().concat("-handle");
        initMq(queueName, routerHandler, null, null);
      }
    }
    return router;
  }

  /**
   * 添加处理器
   */
  @Override
  public Router addMethod(Object delegate, Method method) {
    super.addMethod(delegate, method);
    //注册队列和binding
    if (async) {
      String queueName = method.getDeclaringClass().getName().concat("-").concat(method.getName());
      initMq(queueName, null, delegate, method);
    }
    return router;
  }

  /**
   * 注册rabbitmq相关属性
   */
  private void initMq(String queueName, RouterHandler routerHandler, Object delegate, Method method){
    RouterWithRabbitMq routerWithRabbitMq = (RouterWithRabbitMq) router;
    RabbitAdmin rabbitAdmin = routerWithRabbitMq.getRabbitAdmin();
    //注册队列
    Queue queue = new Queue(queueName);
    rabbitAdmin.declareQueue(queue);
    //注册binding
    Binding binding = BindingBuilder.bind(queue).to(routerWithRabbitMq.getTopicExchange()).with(queueName);
    rabbitAdmin.declareBinding(binding);
    /*
     * 新建简单消息监听容器
     */
    SimpleMessageListenerContainer messageListenerContainer = new SimpleMessageListenerContainer(rabbitAdmin.getRabbitTemplate().getConnectionFactory());
    //监听队列
    messageListenerContainer.addQueueNames(queueName);
    //失败重试次数，次数用完后会删除队列
    messageListenerContainer.setDeclarationRetries(3);
    //失败重试间隔时间
    messageListenerContainer.setFailedDeclarationRetryInterval(10000);
    messageListenerContainer.setRetryDeclarationInterval(10000);
    //当个消费者一次拉取消息的数量
    messageListenerContainer.setPrefetchCount(1);
    //初始化监听器时创建多少个消费者并发消费
    messageListenerContainer.setConcurrentConsumers(1);
    //最大消费者数量
    messageListenerContainer.setMaxConcurrentConsumers(1);
    messageListenerContainer.setDefaultRequeueRejected(false);
    //回调类和方法绑定队列
    messageListenerContainer.setAcknowledgeMode(AcknowledgeMode.MANUAL);
    MessageConverter converter = new SimpleMessageConverter();
    messageListenerContainer.setMessageListener((ChannelAwareMessageListener) (message, channel) -> {
      Map<String, Object> paramMap = (Map<String, Object>) converter.fromMessage(message);
      try {
        if(routerHandler != null){
          routerHandler.handle(paramMap);
        }
        if(method != null){
          method.invoke(delegate, paramMap);
        }
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false); //确认消息成功消费
      } catch (InvocationTargetException e1) {
        router.getRouterExceptionHandler().handle((Exception) e1.getTargetException(), message, channel, paramMap);
//        e1.getTargetException().printStackTrace();
      } catch (Exception e2) {
        router.getRouterExceptionHandler().handle(e2, message, channel, paramMap);
      }
    });
    //启动
    messageListenerContainer.start();
    //添加进列表
    messageListenerContainers.add(messageListenerContainer);
  }
  /**
   * 执行规则内的处理器
   */
  @Override
  public void execute(Map<String, Object> context) {
    super.execute(context);
    RouterWithRabbitMq routerWithRabbitMq = (RouterWithRabbitMq) router;
    if (async) {
      if (!CollectionUtils.isEmpty(handlers)) {
        for (RouterHandler routeHandler : handlers) {
          //写入消息到消息队列
          routerWithRabbitMq.getRabbitAdmin().getRabbitTemplate().convertAndSend("topicExchangeOfRoute", routeHandler.getClass().getName().concat("-handle"), context);
        }
      }
      if (!CollectionUtils.isEmpty(methods)) {
        for (Method method : methods) {
          //写入消息到消息队列
          routerWithRabbitMq.getRabbitAdmin().getRabbitTemplate().convertAndSend("topicExchangeOfRoute", method.getDeclaringClass().getName().concat("-").concat(method.getName()), context);
        }
      }
    }
  }

  /**
   * 销毁
   */
  @Override
  public void destroy() {
    super.destroy();
    for (MessageListenerContainer messageListenerContainer : messageListenerContainers) {
      SimpleMessageListenerContainer s = (SimpleMessageListenerContainer)messageListenerContainer;
      s.destroy();
    }
    messageListenerContainers = null;
  }
}
