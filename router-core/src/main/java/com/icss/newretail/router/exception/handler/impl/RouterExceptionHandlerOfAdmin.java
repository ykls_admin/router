package com.icss.newretail.router.exception.handler.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.amqp.core.Message;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSONObject;
import com.icss.newretail.router.exception.handler.RouterExceptionHandler;
import com.rabbitmq.client.Channel;

import lombok.extern.slf4j.Slf4j;

/**
 * 默认RouterErrorException异常处理器
 */
@Slf4j
public class RouterExceptionHandlerOfAdmin implements RouterExceptionHandler {
  private RestTemplate restTemplate;
  private String routerAdminIp;
  private Integer webPort;
  public RouterExceptionHandlerOfAdmin(RestTemplate restTemplate, String routerAdminIp, Integer webPort){
    this.restTemplate = restTemplate;
    this.routerAdminIp = routerAdminIp;
    this.webPort = webPort;
  }
  @Override
  public void handle(Exception e, Message message, Channel channel, Map<String, Object> paramMap) {
    String exceptionMessage = e.getMessage();
    log.error("RouterExceptionHandlerOfAdmin handler执行出错: ", e);
    StackTraceElement stackTraceElement = e.getStackTrace()[0];
    String className = stackTraceElement.getClassName();
    String methodName = stackTraceElement.getMethodName();
    int lineNumber = stackTraceElement.getLineNumber();
    Map<String, Object> postMap = new HashMap<>();
    postMap.put("exceptionClass", className);
    postMap.put("exceptionMethod", methodName);
    postMap.put("exceptionLine", lineNumber);
    postMap.put("inParam", JSONObject.toJSONString(paramMap));
    postMap.put("exceptionMessage", exceptionMessage);
    try {
      //调用admin保存异常信息，并移除消息
      String result = new RestTemplate().postForObject("http://"+routerAdminIp+":"+webPort+"/router-admin/routerException/saveRouterException", postMap, String.class);
      channel.basicAck(message.getMessageProperties().getDeliveryTag(), false); //确认消息成功消费
    } catch (Exception e1) {
      log.error(e1.getMessage(), e1);
      //调用admin服务出错，重入消息队列
      new Timer().schedule(new TimerTask() {
        public void run() {
          try {
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, true);
          } catch (Exception e1) {
            e1.printStackTrace();
          }
        }
      }, 1000);
    }
  }
}
