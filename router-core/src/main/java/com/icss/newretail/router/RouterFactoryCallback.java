package com.icss.newretail.router;

import com.icss.newretail.router.router.Router;

public interface RouterFactoryCallback {
  Router operation(Router router);
}
