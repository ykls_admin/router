package com.icss.newretail.router.router;

import java.util.List;
import java.util.Map;

import com.icss.newretail.router.checker.RouterDuplicateChecker;
import com.icss.newretail.router.exception.handler.RouterExceptionHandler;
import com.icss.newretail.router.rule.RouterRule;

public interface Router {
  /**
   * 开始一个新的规则
   */
  RouterRule async(boolean async);

  /**
   * 开始一个新的规则
   */
  RouterRule rule();

  /**
   * 获取规则列表
   */
  List<RouterRule> getRouterRuleList();

  /**
   * 获取重复检查器
   */
  RouterDuplicateChecker getRouterDuplicateChecker();

  /**
   * 获取异常处理器
   */
  RouterExceptionHandler getRouterExceptionHandler();

  /**
   * 启动
   */
  void execute(Map<String, Object> context);

  /**
   * 销毁
   */
  void destroy();
}
