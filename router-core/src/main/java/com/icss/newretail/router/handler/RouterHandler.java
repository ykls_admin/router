package com.icss.newretail.router.handler;

import java.util.Map;

/**
 * 路由器的处理类
 */
public interface RouterHandler {

  /**
   * 处理消息.
   *
   * @param context 上下文，handler之间信息传递
   * @return String
   */
  void handle(Map<String, Object> context);
}
