package com.icss.newretail.router.router.impl;

import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitAdmin;

import com.icss.newretail.router.checker.RouterDuplicateChecker;
import com.icss.newretail.router.exception.handler.RouterExceptionHandler;
import com.icss.newretail.router.rule.RouterRule;
import com.icss.newretail.router.rule.impl.RouterRuleWithRabbitMq;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class RouterWithRabbitMq extends AbstractRouter {
  //rabbitAdmin，用于动态创建topicExchange，queue, binding
  private RabbitAdmin rabbitAdmin;
  private TopicExchange topicExchange;
  /**
   * 构造函数
   * @param rabbitAdmin
   */
  public RouterWithRabbitMq(RabbitAdmin rabbitAdmin) {
    super();
    this.rabbitAdmin = rabbitAdmin;
    //注册topicExchange
    TopicExchange topicExchange = new TopicExchange("topicExchangeOfRoute",true,false);
    rabbitAdmin.declareExchange(topicExchange);
    this.topicExchange = topicExchange;
  }
  /**
   * 构造函数
   * @param routerDuplicateChecker
   * @param rabbitAdmin
   */
  public RouterWithRabbitMq(RouterDuplicateChecker routerDuplicateChecker, RabbitAdmin rabbitAdmin) {
    super(routerDuplicateChecker);
    this.rabbitAdmin = rabbitAdmin;
    //注册topicExchange
    TopicExchange topicExchange = new TopicExchange("topicExchangeOfRoute",true,false);
    rabbitAdmin.declareExchange(topicExchange);
    this.topicExchange = topicExchange;
  }
  /**
   * 构造函数
   * @param routerExceptionHandler
   * @param rabbitAdmin
   */
  public RouterWithRabbitMq(RouterExceptionHandler routerExceptionHandler, RabbitAdmin rabbitAdmin) {
    super(routerExceptionHandler);
    this.rabbitAdmin = rabbitAdmin;
    //注册topicExchange
    TopicExchange topicExchange = new TopicExchange("topicExchangeOfRoute",true,false);
    rabbitAdmin.declareExchange(topicExchange);
    this.topicExchange = topicExchange;
  }
  /**
   * 开始一个新的规则
   */
  @Override
  public RouterRule rule() {
    RouterRule routerRule = new RouterRuleWithRabbitMq(this);
    this.routerRuleList.add(routerRule);
    return routerRule;
  }
  /**
   * 销毁
   */
  @Override
  public void destroy() {
    super.destroy();
    this.rabbitAdmin = null;
    this.topicExchange = null;
  }
}
