package com.icss.newretail.test.handler;

import java.util.Map;
import java.util.Random;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * 测试处理类
 * @author sj
 */
@Component
@Slf4j
public class OrderHandler {
  public void handlePay(Map<String, Object> context) {
    log.info("=========支付订单===: {}", context);
//    try {
////      Thread.sleep(20000);
//    } catch (InterruptedException e) {
//      e.printStackTrace();
//    }
  }
  public void handleScore(Map<String, Object> context) {
    log.info("=========处理积分===: {}", context);
    int nextInt = new Random().nextInt(10);
    log.info("=========处理积分nextInt===: {}", nextInt);
//    if(6 > 5){
//      throw new RuntimeException("处理积分 出错了");
//    }
  }
  public void handleMessage(Map<String, Object> context) {
    log.info("=========发送短信===: {}", context);
  }
  public void handleDeliver(Map<String, Object> context) {
    log.info("=========创建配送单===: {}", context);
  }
}
