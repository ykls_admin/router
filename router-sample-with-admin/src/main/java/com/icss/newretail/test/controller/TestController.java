package com.icss.newretail.test.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.icss.newretail.router.RouterFactory;
import com.icss.newretail.router.router.Router;

@RestController
public class TestController {
  @Autowired
  private RouterFactory routerFactory;
  @GetMapping(value = "/test")
  public Map<String, Object> test() {
    Map<String, Object> paramMap = new HashMap<>();
    paramMap.put("orderId", "1111111111");
    Router router = routerFactory.routerFromAdmin("PAY_ORDER");
    router.execute(paramMap);
    return paramMap;
  }
}
