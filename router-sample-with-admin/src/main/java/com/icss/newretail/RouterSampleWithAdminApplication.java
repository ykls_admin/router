package com.icss.newretail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RouterSampleWithAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(RouterSampleWithAdminApplication.class, args);
    }
}

