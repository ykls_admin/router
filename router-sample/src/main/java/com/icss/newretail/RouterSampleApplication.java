package com.icss.newretail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RouterSampleApplication {
    public static void main(String[] args) {
        SpringApplication.run(RouterSampleApplication.class, args);
    }
}

