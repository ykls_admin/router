package com.icss.newretail.test.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.icss.newretail.router.RouterFactory;
import com.icss.newretail.router.router.Router;
import com.icss.newretail.test.handler.TestHandler1;
import com.icss.newretail.test.handler.TestHandler2;
import com.icss.newretail.test.handler.TestHandler3;
import com.icss.newretail.test.handler.TestHandler4;

@RestController
public class Test1Controller {
  @Autowired
  private RouterFactory routerFactory;

  @Autowired
  private TestHandler1 testHandler1;

  @Autowired
  private TestHandler2 testHandler2;

  @Autowired
  private TestHandler3 testHandler3;

  @Autowired
  private TestHandler4 testHandler4;
  @GetMapping(value = "/test")
  public Map<String, Object> test() {
    Map<String, Object> paramMap = new HashMap<>();
    paramMap.put("orderId", "1111111111");
    Router router = routerFactory.router("commitOrder", r -> r
        .async(false).addHandler(testHandler1)
        .async(true).addHandler(testHandler2)
        .async(true).addHandler(testHandler3)
        .async(true).addHandler(testHandler4));
    router.execute(paramMap);
    return paramMap;
  }
}
