package com.icss.newretail.test.handler;

import java.util.Map;
import java.util.Random;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * 测试处理类
 * @author sj
 */
@Component
@Slf4j
public class OrderHandler {
  public void handlePay(Map<String, Object> paramMap) {
    log.info("=========支付订单===: {}", paramMap);
    if(new Random().nextInt(10) > 8){
      throw new RuntimeException("支付订单 出错了");
    }
  }
  public void handleScore(Map<String, Object> paramMap) {
    log.info("=========处理积分===: {}", paramMap);
    int nextInt = new Random().nextInt(10);
    log.info("=========处理积分nextInt===: {}", nextInt);
    if(nextInt > 5){
      throw new RuntimeException("处理积分 出错了");
    }
  }
  public void handleMessage(Map<String, Object> paramMap) {
    log.info("=========发送短信===: {}", paramMap);
  }
  public void handleDeliver(Map<String, Object> paramMap) {
    log.info("=========创建配送单===: {}", paramMap);
  }
}
