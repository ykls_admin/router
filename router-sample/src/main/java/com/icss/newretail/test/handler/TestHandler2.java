package com.icss.newretail.test.handler;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.icss.newretail.router.handler.RouterHandler;

import lombok.extern.slf4j.Slf4j;

/**
 * 测试处理类
 */
@Component
@Slf4j
public class TestHandler2 implements RouterHandler {
  @Override
  public void handle(Map<String, Object> paramMap) {
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    log.info("=========测试2===: {}", paramMap);
//    if(new Random().nextInt(10) > 5){
//      throw new RuntimeException("TestHandler2 出错了");
//    }
    paramMap.put("test2", "1111");
  }
}
