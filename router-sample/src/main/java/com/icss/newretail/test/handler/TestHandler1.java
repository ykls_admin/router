package com.icss.newretail.test.handler;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.icss.newretail.router.handler.RouterHandler;

import lombok.extern.slf4j.Slf4j;

/**
 * 测试处理类
 */
@Component
@Slf4j
public class TestHandler1 implements RouterHandler {
  @Override
  public void handle(Map<String, Object> paramMap) {
    log.info("=========测试1===: {}", paramMap);
    paramMap.put("test1", "1111");
  }
}
