package com.icss.newretail.test.handler;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.icss.newretail.router.handler.RouterHandler;

import lombok.extern.slf4j.Slf4j;

/**
 * 测试处理类
 */
@Component
@Slf4j
public class TestHandler3 implements RouterHandler {
  @Override
  public void handle(Map<String, Object> paramMap) {
    log.info("=========测试3===: {}", paramMap);
//    if(new Random().nextInt(10) > 5){
//      throw new RuntimeException("TestHandler3 出错了");
//    }
  }
}
