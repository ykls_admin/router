package com.icss.newretail.security.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.icss.newretail.role.entity.Role;
import com.icss.newretail.role.service.RoleService;
import com.icss.newretail.user.entity.User;
import com.icss.newretail.user.service.UserService;
import com.icss.newretail.userRole.entity.UserRole;
import com.icss.newretail.userRole.service.UserRoleService;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
  @Autowired
  private UserService userService;
  @Autowired
  private UserRoleService userRoleService;
  @Autowired
  private RoleService roleService;
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = userService.getOne(new QueryWrapper<User>().eq("username", username));
    if (user == null) {
      throw new UsernameNotFoundException("username:" + username + " not found");
    }
    List<UserRole> userRoleList = userRoleService.list(new QueryWrapper<UserRole>().eq("user_id", user.getId()));
    List<GrantedAuthority> grantedAuthoritieList = new ArrayList<>();
    for (int i = 0; i < userRoleList.size(); i++) {
      Role role = roleService.getById(userRoleList.get(i).getRoleId());
      GrantedAuthority grantedAuthority = new SimpleGrantedAuthority("ROLE_".concat(role.getRoleCode()));
      grantedAuthoritieList.add(grantedAuthority);
    }
    return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), grantedAuthoritieList);
  }
}
