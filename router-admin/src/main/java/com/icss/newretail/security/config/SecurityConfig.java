package com.icss.newretail.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.DigestUtils;

import com.icss.newretail.security.handler.UserAccessDeniedHandler;
import com.icss.newretail.security.handler.UserLoginAuthenticationFailureHandler;
import com.icss.newretail.security.handler.UserLoginAuthenticationSuccessHandler;
import com.icss.newretail.security.handler.UserLogoutSuccessHandler;

import lombok.extern.slf4j.Slf4j;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
@Slf4j
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    UserLoginAuthenticationSuccessHandler userLoginAuthenticationSuccessHandler;
    @Autowired
    UserLoginAuthenticationFailureHandler userLoginAuthenticationFailureHandler;
    @Autowired
    UserAccessDeniedHandler userAccessDeniedHandler;
    @Autowired
    UserLogoutSuccessHandler userLogoutSuccessHandler;
    @Autowired
    private UserDetailsService userDetailsService;
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()//禁用csrf 功能
                .headers().frameOptions().sameOrigin()//设置弹出层,等同于response.addHeader("x-frame-options","SAMEORIGIN");
                .and()
                .authorizeRequests()//限定请求
//                .antMatchers("/admin/**").access("hasRole('ADMIN')")//只有管理员才能访问
                .antMatchers("/admin/**").hasRole("ADMIN")//只有管理员才能访问
                .antMatchers("/oauth/**").permitAll()//不需要身份认证
                .antMatchers("/static/**").permitAll()//不需要身份认证
                .antMatchers("/router/getRouterByCode").permitAll()//不需要身份认证
                .antMatchers("/routerException/saveRouterException").permitAll()//不需要身份认证
                .anyRequest().authenticated()//其他路径必须验证身份
//                .and().formLogin()//使用 spring security 默认登录页面
//                .and().httpBasic();//启用http 基础验证
                .and()
                .formLogin()
                .loginPage("/login_page")//自定义登录页面
                .loginProcessingUrl("/login")//spring-security效验的登录url
                .usernameParameter("username")//spring-security效验的账号字段名称
                .passwordParameter("password")//spring-security效验的密码字段名称
                .failureHandler(userLoginAuthenticationFailureHandler)//验证失败处理
                .successHandler(userLoginAuthenticationSuccessHandler)//验证成功处理
                .permitAll()//登录页面不需要验证
                .and()
                .logout().logoutSuccessHandler(userLogoutSuccessHandler)//登出处理
                .permitAll()
                .and()
                .csrf().disable()//禁用csrf 功能
                .exceptionHandling().accessDeniedHandler(userAccessDeniedHandler);
    }
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        //内存中添加一个账号密码角色
//        auth.inMemoryAuthentication()
//                .passwordEncoder(new PasswordEncoder() {
//                    @Override
//                    public String encode(CharSequence arg0) {
//                        return arg0.toString();
//                    }
//                    @Override
//                    public boolean matches(CharSequence arg0, String arg1) {
//                        return arg1.equals(arg0.toString());
//                    }
//                })
//                .withUser(account)
//                .password(password)
//                .roles("USER");
        //账号密码从数据库取
        auth.userDetailsService(userDetailsService).passwordEncoder(new PasswordEncoder() {
            @Override
            public String encode(CharSequence arg0) {
                return DigestUtils.md5DigestAsHex(arg0.toString().getBytes());
            }
            @Override
            public boolean matches(CharSequence arg0, String arg1) {
                return arg1.equals(DigestUtils.md5DigestAsHex(arg0.toString().getBytes()));
            }
        });
    }
}