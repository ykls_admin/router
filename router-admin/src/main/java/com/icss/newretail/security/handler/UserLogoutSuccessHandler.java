package com.icss.newretail.security.handler;

import com.alibaba.fastjson.JSONObject;
import com.icss.newretail.base.vo.ResponseBase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by linziyu on 2019/2/12.
 *
 * 用户登出处理类
 */

@Component("UserLogoutSuccessHandler")
@Slf4j
public class UserLogoutSuccessHandler implements LogoutSuccessHandler{

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        log.info("{}","LogOut*******Success");
        request.getSession().removeAttribute("user");
        ResponseBase responseBase = new ResponseBase(200, "退出成功");
        response.setContentType("application/json;charset=utf-8");
        PrintWriter out = response.getWriter();
        out.write(JSONObject.toJSONString(responseBase));
        out.flush();
        out.close();
    }
}
