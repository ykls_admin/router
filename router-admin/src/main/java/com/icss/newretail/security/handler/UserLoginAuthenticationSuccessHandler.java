package com.icss.newretail.security.handler;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.icss.newretail.base.vo.ResponseBase;
import com.icss.newretail.user.entity.User;
import com.icss.newretail.user.service.UserService;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * 用户验证成功处理类
 */
@Component("UserLoginAuthenticationSuccessHandler")
@Slf4j
public class UserLoginAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
    @Autowired
    private UserService userService;
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        String username = authentication.getName();
        //当前登录用户的角色
        List<GrantedAuthority> grantedAuthoritieList = (List<GrantedAuthority>) authentication.getAuthorities();
        String roleCode = "";
        if (!CollectionUtils.isEmpty(grantedAuthoritieList)) {
            roleCode = grantedAuthoritieList.get(0).getAuthority();
        }
        User user = userService.getOne(new QueryWrapper<User>().eq("username", username));
        user.setRoleCode(roleCode);
        request.getSession().setAttribute("user", user);
        ResponseBase responseBase = new ResponseBase(200, "登录成功");
        response.setContentType("application/json;charset=utf-8");
        PrintWriter out = response.getWriter();
        out.write(JSONObject.toJSONString(responseBase));
        out.flush();
        out.close();
    }
}
