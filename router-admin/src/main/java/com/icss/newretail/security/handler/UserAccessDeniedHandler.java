package com.icss.newretail.security.handler;

import com.alibaba.fastjson.JSONObject;
import com.icss.newretail.base.vo.ResponseBase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by linziyu on 2019/2/13.
 *
 * 无权限操作时的处理类
 */

@Component("UserAuthenticationAccessDeniedHandler")
@Slf4j
public class UserAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response,
                       AccessDeniedException e) throws IOException, ServletException {
        log.info("{}","无权限处理");
        ResponseBase responseBase = new ResponseBase(500, "error");
        response.setContentType("application/json;charset=utf-8");
        PrintWriter out = response.getWriter();
        out.write(JSONObject.toJSONString(responseBase));
        out.flush();
        out.close();
    }
}
