package com.icss.newretail.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DbUtil {
  /**
   * 获取数据源连接
   */
  public static Connection getConnection(String datasourceType, String jdbcUrl, String username, String password){
    String driverClass = "";
    if ("mysql".equals(datasourceType)) {
      driverClass = "com.mysql.jdbc.Driver";
    } else if ("oracle".equals(datasourceType)){
      driverClass = "";
    } else if ("db2".equals(datasourceType)){
      driverClass = "com.ibm.db2.jcc.DB2Driver";
    }
    Connection conn = null;
    try {
      //加载驱动
      Class.forName(driverClass).newInstance();
      //获取连接
      conn = DriverManager.getConnection(jdbcUrl, username, password);
    } catch (Exception e) {
      e.printStackTrace();
    } finally {

    }
    return conn;
  }
  /**
   * 执行查询SQL
   */
  public static String getOne(Connection conn, String sql){
    String result = "";
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    try {
      //执行SQL
      pstmt = conn.prepareStatement(sql);
      //获取结果
      rs = pstmt.executeQuery();
      if (rs != null) {
        if (rs.next()) {
          result = rs.getString(1);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally{
      if (rs != null){
        try {
          rs.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
      if (pstmt != null){
        try {
          pstmt.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
      if (conn != null){
        try {
          conn.setAutoCommit(true);
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return result;
  }
  /**
   * 执行存储过程
   */
  public static List<String> callProcedure(Connection conn, String procedureName, String[] procedureParams) throws Exception{
    List<String> resultList = new ArrayList<>();
    String paramsStr = "";
    for (int i=0; i<procedureParams.length; i++) {
      if (i == 0) {
        paramsStr += "?";
      } else {
        paramsStr += ",?";
      }
    }
    //调用格式 {call 存储过程名(参数)}
    CallableStatement cst = conn.prepareCall("{call "+procedureName+"("+paramsStr+")}");
    for (int i=0; i<procedureParams.length; i++) {
      cst.setObject(i+1, procedureParams[i].trim());
    }
    //获取结果
    ResultSet rs = cst.executeQuery();
    if (rs != null) {
      ResultSetMetaData resultSetMetaData = rs.getMetaData();
      int columnCount = resultSetMetaData.getColumnCount();
      String[] results = new String[columnCount];
      while (rs.next()) {
        for (int i=0; i<columnCount; i++) {
          results[i] = rs.getString(i+1);
        }
        resultList.add(Arrays.toString(results));
      }
    }
    rs.close();
    cst.close();
    conn.setAutoCommit(true);
    conn.close();
    return resultList;
  }
}
