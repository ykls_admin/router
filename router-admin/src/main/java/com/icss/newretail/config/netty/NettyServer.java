package com.icss.newretail.config.netty;

import org.springframework.scheduling.annotation.Async;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NettyServer {
  //监听线程组，监听客户请求
  private EventLoopGroup bossGroup;
  //工作线程组，负责处理与客户端的数据通讯
  private EventLoopGroup workerGroup;
  //channel
  private Channel channel;
  //channelGroup
  private ChannelGroup channelGroup;
  //服务端监听端口号
  private Integer port;
  public NettyServer (ChannelGroup channelGroup, Integer port) {
    this.channelGroup = channelGroup;
    this.port = port;
  }
  @Async
  public void start() {
    bossGroup = new NioEventLoopGroup();
    workerGroup = new NioEventLoopGroup();
    ServerBootstrap bootstrap = new ServerBootstrap()
        .group(bossGroup, workerGroup)
        .channel(NioServerSocketChannel.class)
        .childHandler(new ChannelInitializer<SocketChannel>() {
          @Override
          protected void initChannel(SocketChannel socketChannel) throws Exception {
            ChannelPipeline pipeline = socketChannel.pipeline();
            pipeline.addLast("decoder", new StringDecoder(CharsetUtil.UTF_8));
            pipeline.addLast("encoder", new StringEncoder(CharsetUtil.UTF_8));
            pipeline.addLast(new NettyServerHandler(channelGroup));
          }
        })
        .localAddress(port)
        //设置队列大小
        .option(ChannelOption.SO_BACKLOG, 1024)
        //允许复用端口
        .option(ChannelOption.SO_REUSEADDR, true)
        // 两小时内没有数据的通信时,TCP会自动发送一个活动探测数据报文
        .childOption(ChannelOption.SO_KEEPALIVE, true);
    try {
      log.info("netty server start................");
      ChannelFuture future = bootstrap.bind(port).sync();
      //这里绑定端口启动后，会阻塞主线程
      channel = future.channel().closeFuture().sync().channel();
    } catch (InterruptedException e) {
      log.error("netty server error................", e);
    } finally {
      log.info("netty server close................");
      workerGroup.shutdownGracefully();
      bossGroup.shutdownGracefully();
    }
  }
}
