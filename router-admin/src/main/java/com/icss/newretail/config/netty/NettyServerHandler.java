package com.icss.newretail.config.netty;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.group.ChannelGroup;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NettyServerHandler extends ChannelInboundHandlerAdapter {
  private ChannelGroup channelGroup;
  public NettyServerHandler(ChannelGroup channelGroup){
    this.channelGroup = channelGroup;
  }
  /**
   * 客户端连接会触发
   */
  @Override
  public void channelActive(ChannelHandlerContext ctx) throws Exception {
    log.info("server channel active......");
    channelGroup.add(ctx.channel());
  }

  /**
   * 客户端发消息会触发
   */
  @Override
  public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
    if(!"heat".equals(msg.toString())){
      log.info("server channel read......: {}", msg.toString());
    }
  }

  /**
   * 发生异常触发
   */
  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
    log.error("server exception......", cause);
    channelGroup.remove(ctx.channel());
    ctx.close();
  }
}