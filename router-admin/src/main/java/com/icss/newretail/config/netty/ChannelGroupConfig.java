package com.icss.newretail.config.netty;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

@Component
public class ChannelGroupConfig {
  @Bean
  public ChannelGroup channelGroup(){
    return new DefaultChannelGroup("ChannelGroups", GlobalEventExecutor.INSTANCE);
  }
}
