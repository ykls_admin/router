package com.icss.newretail.config.netty;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.netty.channel.group.ChannelGroup;

@Configuration
public class NettyServerConfig {
  @Value("${router.admin.socketPort}")
  private Integer routerAdminPort;
  @Autowired
  private ChannelGroup channelGroup;
  @Bean
  public NettyServer nettyServer(){
    return new NettyServer(channelGroup, routerAdminPort);
  }

  @Autowired
  private NettyServer nettyServer;
  @PostConstruct
  public void init(){
    nettyServer.start();
  }
}
