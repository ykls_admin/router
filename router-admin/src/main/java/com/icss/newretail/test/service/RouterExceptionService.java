package com.icss.newretail.test.service;

import com.icss.newretail.test.entity.RouterException;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 路由异常跟踪表 服务类
 * </p>
 *
 * @author 
 * @since 2020-06-02
 */
public interface RouterExceptionService extends IService<RouterException> {

}
