package com.icss.newretail.test.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 路由方案表
 * </p>
 *
 * @author 
 * @since 2020-06-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Router对象", description="路由方案表")
public class Router implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId("ROUTER_ID")
    private String routerId;

    @ApiModelProperty(value = "方案编码")
    @TableField("ROUTER_CODE")
    private String routerCode;

    @ApiModelProperty(value = "方案名称")
    @TableField("ROUTER_NAME")
    private String routerName;

    @ApiModelProperty(value = "异常处理类型MQ, DB")
    @TableField("HANDLE_TYPE")
    private String handleType;

    @ApiModelProperty(value = "状态1启用0禁用")
    @TableField("STATUS")
    private Integer status;

    @ApiModelProperty(value = "创建时间")
    @TableField("CREATE_TIME")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建用户")
    @TableField("CREATE_USER")
    private String createUser;

    @ApiModelProperty(value = "更新时间")
    @TableField("UPDATE_TIME")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "更新用户")
    @TableField("UPDATE_USER")
    private String updateUser;

    @TableField(exist = false)
    private List<RouterRule> routerRuleList;
}
