package com.icss.newretail.test.dao;

import com.icss.newretail.test.entity.Router;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 路由方案表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-06-02
 */
public interface RouterMapper extends BaseMapper<Router> {

}
