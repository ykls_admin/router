package com.icss.newretail.test.service;

import com.icss.newretail.test.entity.RouterLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 路由日志表 服务类
 * </p>
 *
 * @author 
 * @since 2020-06-02
 */
public interface RouterLogService extends IService<RouterLog> {

}
