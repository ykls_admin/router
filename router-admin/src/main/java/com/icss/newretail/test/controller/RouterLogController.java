package com.icss.newretail.test.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.icss.newretail.test.service.RouterLogService;

/**
 * <p>
 * 路由日志表 前端控制器
 * </p>
 *
 * @author 
 * @since 2020-06-02
 */
@Controller
@RequestMapping("/routerLog")
public class RouterLogController {
  @Autowired
  private RouterLogService routerLogService;
}
