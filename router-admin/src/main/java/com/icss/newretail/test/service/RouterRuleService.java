package com.icss.newretail.test.service;

import java.util.List;

import com.icss.newretail.test.entity.RouterRule;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 路由规则表 服务类
 * </p>
 *
 * @author 
 * @since 2020-06-02
 */
public interface RouterRuleService extends IService<RouterRule> {
  List<RouterRule> getRouterRuleListByRouterCode(String routerCode);
}
