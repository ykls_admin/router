package com.icss.newretail.test.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.icss.newretail.base.vo.ResponseBase;
import com.icss.newretail.base.vo.ResponseLayui;
import com.icss.newretail.test.entity.RouterRule;
import com.icss.newretail.test.service.RouterRuleService;

/**
 * <p>
 * 路由规则表 前端控制器
 * </p>
 *
 * @author 
 * @since 2020-06-02
 */
@Controller
@RequestMapping("/routerRule")
public class RouterRuleController {
  @Autowired
  private RouterRuleService routerRuleService;

  /**
   * 获取规则列表
   * @return
   */
  @GetMapping(value = "/getRouterRuleList")
  @ResponseBody
  ResponseLayui<RouterRule> getRouterRuleList(
      @RequestParam(required = true) String routerCode) {
    List<RouterRule> routerRuleList = routerRuleService.getRouterRuleListByRouterCode(routerCode);
    return new ResponseLayui<RouterRule>().setCode(0).setMsg("").setData(routerRuleList).setCount(Long.valueOf(routerRuleList.size()));
  }

  /**
   * 获取规则列表
   * @return
   */
  @GetMapping(value = "/getRouterRuleListByRouterCode")
  @ResponseBody
  List<RouterRule> getRouterRuleListByRouterCode(
      @RequestParam(required = true) String routerCode) {
    List<RouterRule> routerRuleList = routerRuleService.getRouterRuleListByRouterCode(routerCode);
    return routerRuleList;
  }

  /**
   * 删除规则
   * @param ruleId
   * @return
   */
  @PostMapping(value = "deleteRouter")
  @ResponseBody
  public ResponseBase deleteRouter(@RequestParam(value = "ruleId") String ruleId) {
    routerRuleService.removeById(ruleId);
    return new ResponseBase(200, "删除成功");
  }
}
