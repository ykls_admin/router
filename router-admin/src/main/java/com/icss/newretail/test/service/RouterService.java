package com.icss.newretail.test.service;

import com.icss.newretail.test.entity.Router;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 路由方案表 服务类
 * </p>
 *
 * @author 
 * @since 2020-06-02
 */
public interface RouterService extends IService<Router> {
  
}
