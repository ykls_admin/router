package com.icss.newretail.test.dao;

import com.icss.newretail.test.entity.RouterException;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 路由异常跟踪表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-06-02
 */
public interface RouterExceptionMapper extends BaseMapper<RouterException> {

}
