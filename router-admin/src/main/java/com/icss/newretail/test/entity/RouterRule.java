package com.icss.newretail.test.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 路由规则表
 * </p>
 *
 * @author 
 * @since 2020-06-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="RouterRule对象", description="路由规则表")
public class RouterRule implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId("RULE_ID")
    private String ruleId;

    @ApiModelProperty(value = "方案ID")
    @TableField("ROUTER_ID")
    private String routerId;

    @ApiModelProperty(value = "规则名称")
    @TableField("RULE_NAME")
    private String ruleName;

    @ApiModelProperty(value = "类名")
    @TableField("CLASS_NAME")
    private String className;

    @ApiModelProperty(value = "方法名")
    @TableField("METHOD_NAME")
    private String methodName;

    @ApiModelProperty(value = "是否异步执行")
    @TableField("ASYNC")
    private Integer async;

    @ApiModelProperty(value = "排序号")
    @TableField("SORT_NUM")
    private Integer sortNum;

    @ApiModelProperty(value = "状态1启用0禁用")
    @TableField("STATUS")
    private Integer status;

    @ApiModelProperty(value = "创建时间")
    @TableField("CREATE_TIME")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建用户")
    @TableField("CREATE_USER")
    private String createUser;

    @ApiModelProperty(value = "更新时间")
    @TableField("UPDATE_TIME")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "更新用户")
    @TableField("UPDATE_USER")
    private String updateUser;

    @TableField(exist = false)
    private Integer index;
}
