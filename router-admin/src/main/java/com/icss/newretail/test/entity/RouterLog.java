package com.icss.newretail.test.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 路由日志表
 * </p>
 *
 * @author 
 * @since 2020-06-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="RouterLog对象", description="路由日志表")
public class RouterLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId("ID")
    private String id;

    @ApiModelProperty(value = "线程")
    @TableField("THREAD")
    private String thread;

    @ApiModelProperty(value = "类名")
    @TableField("CLASS_NAME")
    private String className;

    @ApiModelProperty(value = "方法名")
    @TableField("METHOD_NAME")
    private String methodName;

    @ApiModelProperty(value = "入参")
    @TableField("IN_PARAM")
    private String inParam;

    @ApiModelProperty(value = "执行时间")
    @TableField("BEG_TIME")
    private Long begTime;

    @ApiModelProperty(value = "结束时间")
    @TableField("END_TIME")
    private Long endTime;

    @ApiModelProperty(value = "执行时长")
    @TableField("DURATION")
    private Long duration;

    @ApiModelProperty(value = "状态1启用0禁用")
    @TableField("STATUS")
    private Integer status;

    @ApiModelProperty(value = "创建时间")
    @TableField("CREATE_TIME")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建用户")
    @TableField("CREATE_USER")
    private String createUser;

    @ApiModelProperty(value = "更新时间")
    @TableField("UPDATE_TIME")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "更新用户")
    @TableField("UPDATE_USER")
    private String updateUser;


}
