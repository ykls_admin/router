package com.icss.newretail.test.dao;

import java.util.List;

import com.icss.newretail.test.entity.RouterRule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 路由规则表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-06-02
 */
public interface RouterRuleMapper extends BaseMapper<RouterRule> {
  List<RouterRule> getRouterRuleListByRouterCode(String routerCode);
}
