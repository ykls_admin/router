package com.icss.newretail.test.controller;


import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.icss.newretail.base.vo.ResponseBase;
import com.icss.newretail.base.vo.ResponseLayui;
import com.icss.newretail.base.vo.ResponseResult;
import com.icss.newretail.test.entity.RouterException;
import com.icss.newretail.test.service.RouterExceptionService;

/**
 * <p>
 * 路由异常跟踪表 前端控制器
 * </p>
 *
 * @author 
 * @since 2020-06-02
 */
@Controller
@RequestMapping("/routerException")
public class RouterExceptionController {
  @Autowired
  private RouterExceptionService routerExceptionService;
  @Autowired
  private RabbitAdmin rabbitAdmin;
  /**
   * 路由异常列表页面
   *
   * @return
   */
  @GetMapping("/routerExceptionList")
  public String routerExceptionList(Model model) {
    return "routerException/routerExceptionList";
  }

  /**
   * 路由异常列表
   *
   * @return
   */
  @PostMapping("/getRouterExceptionList")
  @ResponseBody
  public ResponseLayui<RouterException> getRouterExceptionList(Integer page, Integer limit,
      @RequestParam(value = "exceptionClass") String exceptionClass,
      @RequestParam(value = "exceptionMethod") String exceptionMethod,
      @RequestParam(value = "inParam") String inParam,
      @RequestParam(value = "handleStatus") Integer handleStatus) {
    //当前登录用户的角色
    List<GrantedAuthority> grantedAuthoritieList = (List<GrantedAuthority>) SecurityContextHolder.getContext()
        .getAuthentication().getAuthorities();
    String roleCode = "";
    if (!CollectionUtils.isEmpty(grantedAuthoritieList)) {
      roleCode = grantedAuthoritieList.get(0).getAuthority();
    }
    IPage<RouterException> routerExceptionPage = routerExceptionService.page(new Page<>(page, limit),
        new QueryWrapper<RouterException>()
            .like(!StringUtils.isEmpty(exceptionClass), "exception_class", exceptionClass)
            .like(!StringUtils.isEmpty(exceptionMethod), "exception_method", exceptionMethod)
            .like(!StringUtils.isEmpty(inParam), "in_param", inParam)
            .eq(handleStatus != null && handleStatus != -1,"handle_status", handleStatus)
            .orderByAsc("handle_status").orderByDesc("create_time"));
    return new ResponseLayui<RouterException>().setCode(0).setMsg("").setData(routerExceptionPage.getRecords()).setCount(routerExceptionPage.getTotal());
  }

  /**
   * 保存路由异常
   * @param routerException
   * @return
   */
  @PostMapping(value = "/saveRouterException")
  @ResponseBody
  ResponseBase saveRouterException (@RequestBody RouterException routerException) {
    routerException.setId(UUID.randomUUID().toString());
    routerException.setHandleStatus(0);
    routerException.setStatus(1);
    routerException.setCreateTime(LocalDateTime.now());
    routerException.setUpdateTime(LocalDateTime.now());
    routerExceptionService.save(routerException);
    return new ResponseBase(200, "保存成功");
  }
  
  /**
   * 删除路由异常
   * @param id
   * @return
   */
  @PostMapping(value = "deleteRouterException")
  @ResponseBody
  public ResponseBase deleteRouterException(@RequestParam(value = "id") String id) {
    routerExceptionService.removeById(id);
    return new ResponseBase(200, "删除成功");
  }

  /**
   * 标记已处理
   * @param id
   * @return
   */
  @PostMapping(value = "handleRouterException")
  @ResponseBody
  public ResponseBase handleRouterException(@RequestParam(value = "id") String id) {
    RouterException routerExceptionDb = routerExceptionService.getById(id);
    if(routerExceptionDb.getHandleStatus() == 1 || routerExceptionDb.getHandleStatus() == 2){
      return new ResponseBase(500, "该记录已处理，请勿重复操作");
    }
    RouterException routerException = new RouterException();
    routerException.setId(id);
    routerException.setHandleStatus(1);
    routerException.setUpdateTime(LocalDateTime.now());
    routerExceptionService.updateById(routerException);
    return new ResponseBase(200, "标记已处理成功");
  }

  /**
   * 放回消息队列
   * @param id
   * @return
   */
  @PostMapping(value = "backMq")
  @ResponseBody
  public ResponseBase backMq(@RequestParam(value = "id") String id) {
    RouterException routerExceptionDb = routerExceptionService.getById(id);
    if(routerExceptionDb.getHandleStatus() == 1 || routerExceptionDb.getHandleStatus() == 2){
      return new ResponseBase(500, "该记录已处理，请勿重复操作");
    }
    Map<String, Object> context = JSONObject.parseObject(routerExceptionDb.getInParam(), Map.class);
    rabbitAdmin.getRabbitTemplate().convertAndSend("topicExchangeOfRoute", routerExceptionDb.getExceptionClass().concat("-").concat(routerExceptionDb.getExceptionMethod()), context);
    RouterException routerException = new RouterException();
    routerException.setId(id);
    routerException.setHandleStatus(2);
    routerException.setUpdateTime(LocalDateTime.now());
    routerExceptionService.updateById(routerException);
    return new ResponseBase(200, "放回消息队列成功");
  }

  /**
   * 根据id获取路由异常
   * @param model
   * @param id
   * @return
   */
  @RequestMapping(value = "getRouterExceptionById")
  @ResponseBody
  public ResponseResult<RouterException> getRouterExceptionById(Model model, @RequestParam(value = "id") String id) {
    RouterException routerException = routerExceptionService.getById(id);
    return new ResponseResult<RouterException>(200, "", routerException);
  }
  
}
