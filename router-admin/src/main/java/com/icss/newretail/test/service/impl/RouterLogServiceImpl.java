package com.icss.newretail.test.service.impl;

import com.icss.newretail.test.entity.RouterLog;
import com.icss.newretail.test.dao.RouterLogMapper;
import com.icss.newretail.test.service.RouterLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 路由日志表 服务实现类
 * </p>
 *
 * @author 
 * @since 2020-06-02
 */
@Service
public class RouterLogServiceImpl extends ServiceImpl<RouterLogMapper, RouterLog> implements RouterLogService {

}
