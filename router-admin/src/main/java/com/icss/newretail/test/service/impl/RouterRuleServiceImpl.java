package com.icss.newretail.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.icss.newretail.test.dao.RouterRuleMapper;
import com.icss.newretail.test.entity.RouterRule;
import com.icss.newretail.test.service.RouterRuleService;

/**
 * <p>
 * 路由规则表 服务实现类
 * </p>
 *
 * @author 
 * @since 2020-06-02
 */
@Service
public class RouterRuleServiceImpl extends ServiceImpl<RouterRuleMapper, RouterRule> implements RouterRuleService {
  @Autowired
  private RouterRuleMapper routerRuleMapper;

  @Override
  public List<RouterRule> getRouterRuleListByRouterCode(String routerCode) {
    return routerRuleMapper.getRouterRuleListByRouterCode(routerCode);
  }
}
