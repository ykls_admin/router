package com.icss.newretail.test.dao;

import com.icss.newretail.test.entity.RouterLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 路由日志表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-06-02
 */
public interface RouterLogMapper extends BaseMapper<RouterLog> {

}
