package com.icss.newretail.test.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 路由异常跟踪表
 * </p>
 *
 * @author 
 * @since 2020-06-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="RouterException对象", description="路由异常跟踪表")
public class RouterException implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId("ID")
    private String id;

    @ApiModelProperty(value = "异常类")
    @TableField("EXCEPTION_CLASS")
    private String exceptionClass;

    @ApiModelProperty(value = "异常方法")
    @TableField("EXCEPTION_METHOD")
    private String exceptionMethod;

    @ApiModelProperty(value = "异常行号")
    @TableField("EXCEPTION_LINE")
    private Integer exceptionLine;

    @ApiModelProperty(value = "入参")
    @TableField("IN_PARAM")
    private String inParam;

    @ApiModelProperty(value = "异常信息")
    @TableField("EXCEPTION_MESSAGE")
    private String exceptionMessage;

    @ApiModelProperty(value = "处理状态1已处理0未处理")
    @TableField("HANDLE_STATUS")
    private Integer handleStatus;

    @ApiModelProperty(value = "状态1启用0禁用")
    @TableField("STATUS")
    private Integer status;

    @ApiModelProperty(value = "创建时间")
    @TableField("CREATE_TIME")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建用户")
    @TableField("CREATE_USER")
    private String createUser;

    @ApiModelProperty(value = "更新时间")
    @TableField("UPDATE_TIME")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "更新用户")
    @TableField("UPDATE_USER")
    private String updateUser;


}
