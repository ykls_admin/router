package com.icss.newretail.test.service.impl;

import com.icss.newretail.test.entity.RouterException;
import com.icss.newretail.test.dao.RouterExceptionMapper;
import com.icss.newretail.test.service.RouterExceptionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 路由异常跟踪表 服务实现类
 * </p>
 *
 * @author 
 * @since 2020-06-02
 */
@Service
public class RouterExceptionServiceImpl extends ServiceImpl<RouterExceptionMapper, RouterException> implements RouterExceptionService {

}
