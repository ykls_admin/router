package com.icss.newretail.test.service.impl;

import com.icss.newretail.test.entity.Router;
import com.icss.newretail.test.dao.RouterMapper;
import com.icss.newretail.test.service.RouterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 路由方案表 服务实现类
 * </p>
 *
 * @author 
 * @since 2020-06-02
 */
@Service
public class RouterServiceImpl extends ServiceImpl<RouterMapper, Router> implements RouterService {

}
