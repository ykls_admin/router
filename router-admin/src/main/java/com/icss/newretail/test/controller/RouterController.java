package com.icss.newretail.test.controller;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.icss.newretail.base.vo.ResponseBase;
import com.icss.newretail.base.vo.ResponseLayui;
import com.icss.newretail.base.vo.ResponseResult;
import com.icss.newretail.test.entity.Router;
import com.icss.newretail.test.entity.RouterRule;
import com.icss.newretail.test.service.RouterRuleService;
import com.icss.newretail.test.service.RouterService;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.ChannelGroupFuture;
import io.netty.channel.group.ChannelGroupFutureListener;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * 路由方案表 前端控制器
 * </p>
 *
 * @author 
 * @since 2020-06-02
 */
@Slf4j
@Controller
@RequestMapping("/router")
public class RouterController {
  @Autowired
  private RouterService routerService;
  @Autowired
  private RouterRuleService routerRuleService;
  @Autowired
  private ChannelGroup channelGroup;

  /**
   * 路由方案列表页面
   *
   * @return
   */
  @GetMapping("/routerList")
  public String routerList(Model model) {
    return "router/routerList";
  }

  /**
   * 路由方案列表
   *
   * @return
   */
  @PostMapping("/getRouterList")
  @ResponseBody
  public ResponseLayui<Router> getRouterList(Integer page, Integer limit,
      @RequestParam(value = "routerNameQuery") String routerNameQuery) {
    //当前登录用户的角色
    List<GrantedAuthority> grantedAuthoritieList = (List<GrantedAuthority>) SecurityContextHolder.getContext()
        .getAuthentication().getAuthorities();
    String roleCode = "";
    if (!CollectionUtils.isEmpty(grantedAuthoritieList)) {
      roleCode = grantedAuthoritieList.get(0).getAuthority();
    }
    IPage<Router> routerPage = routerService.page(new Page<>(page, limit),
        new QueryWrapper<Router>()
            .like(!StringUtils.isEmpty(routerNameQuery), "router_name", routerNameQuery)
            .orderByAsc("router_code"));
    return new ResponseLayui<Router>().setCode(0).setMsg("").setData(routerPage.getRecords()).setCount(routerPage.getTotal());
  }

  /**
   * 添加路由方案页面
   *
   * @return
   */
  @GetMapping("/routerAdd")
  public String routerAdd() {
    return "router/routerAdd";
  }

  /**
   * 修改路由方案页面
   * @param model
   * @param routerId
   * @return
   */
  @GetMapping(value = "routerEdit")
  public String routerEdit(Model model, @RequestParam(value = "routerId") String routerId) {
    Router router = routerService.getById(routerId);
    List<RouterRule> routerRuleList = routerRuleService.list(new QueryWrapper<RouterRule>().eq("router_id", routerId).orderByAsc("sort_num"));
    for (int i = 0; i < routerRuleList.size(); i++) {
      routerRuleList.get(i).setIndex(i);
    }
    router.setRouterRuleList(routerRuleList);
    model.addAttribute("router", router);
    model.addAttribute("routerRuleList", routerRuleList);
    model.addAttribute("routerId", routerId);
    return "router/routerEdit";
  }

  /**
   * 添加路由方案
   *
   * @return
   */
  @PostMapping(value = "addRouter")
  @ResponseBody
  public ResponseBase addRouter(@RequestBody Router router) {
    Router routerCheck = routerService.getOne(new QueryWrapper<Router>().eq("router_code", router.getRouterCode()));
    if (routerCheck != null) {
      return new ResponseBase(500, "路由方案编码重复");
    }
    String routerId = UUID.randomUUID().toString();
    router.setRouterId(routerId);
    router.setStatus(1);
    router.setCreateUser(SecurityContextHolder.getContext().getAuthentication().getName());
    router.setCreateTime(LocalDateTime.now());
    router.setUpdateTime(LocalDateTime.now());
    //保存数据库
    routerService.save(router);
    List<RouterRule> routerRuleList = router.getRouterRuleList();
    for (RouterRule routerRule:routerRuleList) {
      routerRule.setRuleId(UUID.randomUUID().toString());
      routerRule.setRouterId(routerId);
      routerRule.setStatus(1);
      routerRule.setCreateUser(SecurityContextHolder.getContext().getAuthentication().getName());
      routerRule.setCreateTime(LocalDateTime.now());
      routerRule.setUpdateTime(LocalDateTime.now());
    }
    routerRuleService.saveBatch(routerRuleList);
    return new ResponseBase(200, "保存成功");
  }

  /**
   * 修改路由方案
   * @param router
   * @return
   */
  @PostMapping(value = "updateRouter")
  @ResponseBody
  public ResponseBase updateRouter(@RequestBody Router router) {
    router.setUpdateUser(SecurityContextHolder.getContext().getAuthentication().getName());
    router.setUpdateTime(LocalDateTime.now());
    //保存数据库
    routerService.updateById(router);
    List<RouterRule> routerRuleList = router.getRouterRuleList();
    for (RouterRule routerRule:routerRuleList) {
      routerRule.setUpdateUser(SecurityContextHolder.getContext().getAuthentication().getName());
      routerRule.setUpdateTime(LocalDateTime.now());
    }
    routerRuleService.updateBatchById(routerRuleList);
    return new ResponseBase(200, "保存成功");
  }

  /**
   * 删除路由方案
   * @param routerId
   * @return
   */
  @PostMapping(value = "deleteRouter")
  @ResponseBody
  public ResponseBase deleteRouter(@RequestParam(value = "routerId") String routerId) {
    routerService.removeById(routerId);
    List<RouterRule> routerRuleList = routerRuleService.list(new QueryWrapper<RouterRule>().eq("router_id", routerId).orderByAsc("sort_num"));
    List<String> ruleIdList = new ArrayList<>();
    for (RouterRule routerRule:routerRuleList){
      ruleIdList.add(routerRule.getRuleId());
    }
    routerService.removeByIds(ruleIdList);
    return new ResponseBase(200, "删除成功");
  }

  /**
   * 根据id获取路由方案
   * @param routerId
   * @return
   */
  @RequestMapping(value = "getRouterById")
  @ResponseBody
  public ResponseResult<Router> getRouterById(@RequestParam(value = "routerId") String routerId) {
    Router router = routerService.getById(routerId);
    List<RouterRule> routerRuleList = routerRuleService.list(new QueryWrapper<RouterRule>().eq("router_id", routerId).orderByAsc("sort_num"));
    router.setRouterRuleList(routerRuleList);
    return new ResponseResult<Router>(200, "", router);
  }

  /**
   * 根据code获取路由方案
   * @param routerCode
   * @return
   */
  @RequestMapping(value = "getRouterByCode")
  @ResponseBody
  public Router getRouterByCode(@RequestParam(value = "routerCode") String routerCode) {
    Router router = routerService.getOne(new QueryWrapper<Router>().eq("router_code", routerCode));
    List<RouterRule> routerRuleList = routerRuleService.getRouterRuleListByRouterCode(routerCode);
    router.setRouterRuleList(routerRuleList);
    return router;
  }

  /**
   * 下发方案到客户端
   * @return
   */
  @GetMapping(value = "/sendRouter")
  @ResponseBody
  ResponseBase sendRouter(String routerCode){
    if (channelGroup.isEmpty()){
      return new ResponseBase(500, "没有客户端在线");
    }
    ChannelGroupFuture channelGroupFuture = channelGroup.writeAndFlush("routerKey:" + routerCode);
    log.info("channelGroupFuture.isSuccess(): {}", channelGroupFuture.isSuccess());
    channelGroupFuture.addListener(new ChannelGroupFutureListener() {
      @Override
      public void operationComplete(ChannelGroupFuture channelFutures) throws Exception {
        for (ChannelFuture f: channelFutures) {
          f.addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture channelFuture) throws Exception {
              log.info("channelFuture.isSuccess: {}", channelFuture.isSuccess());
            }
          });
        }
      }
    });
    log.info("channelGroupFuture.isSuccess(): {}", channelGroupFuture.isSuccess());
    return new ResponseBase(200, "发送成功");
  }

  /**
   * 获取客户端实例列表
   * @param routerCode
   * @return
   */
  @RequestMapping(value = "getClientList")
  @ResponseBody
  public ResponseLayui<String> getClientList(@RequestParam(value = "routerCode") String routerCode) {
    if (channelGroup.isEmpty()){
      return new ResponseLayui<String>().setCode(500).setMsg("没有客户端在线");
    }
    List<String> clientList = new ArrayList<>();
    Iterator<Channel> it = channelGroup.iterator();
    while (it.hasNext()) {
      Channel channel = it.next();
      log.info("channel: {}", channel);
      clientList.add(channel.toString());
    }
    return new ResponseLayui<String>().setCode(200).setMsg("获取成功").setCount(Long.valueOf(channelGroup.size())).setData(clientList);
  }
}
