package com.icss.newretail.base.vo;

import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
@Data
@RequiredArgsConstructor
@Accessors(chain = true)
public class ResponseLayui<T> {
    @NotNull
    private Integer code;
    @NotNull
    private String msg;
    private Long count;
    private List<T> data;
}
