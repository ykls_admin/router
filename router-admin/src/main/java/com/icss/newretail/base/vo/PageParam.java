package com.icss.newretail.base.vo;

/**
 * 分页参数工具,处理分页参数
 * 
 * @author ysjiang
 * 
 */
public class PageParam {

	/**
	 * 页码
	 */
	private int page = 1;

	/**
	 * 每页的数量
	 */
	private int rows = 10;
	
	private int limit = 30;// layui的rows

	private String sort = "";
	private String order = "";

	public int getPage() {
		if (page == 0) {
			page = 1;
		}
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		if (rows == 0) {
			rows = 10;
		}
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	/**
	 * 取得分页的开始记录位置
	 * 
	 * @return
	 */
	public int getBeginIndex() {
		return (getPage() - 1) * rows;
	}
	
	public int getEndIndex() {
		return getPage() * rows;
	}

	/**
	 * 取得每页的记录数量
	 * 
	 * @return
	 */
	public int getRowCount() {
		return rows;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
		this.rows = limit;
	}

}
