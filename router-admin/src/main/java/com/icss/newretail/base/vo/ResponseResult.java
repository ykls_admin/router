package com.icss.newretail.base.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * Response-返回标识
 */
@Data
@AllArgsConstructor
@Accessors(chain = true)
public class ResponseResult<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @ApiModelProperty(value = "返回状态（1:成功 0:失败）")
    private int code;// 1:成功 0:失败 
    
    @ApiModelProperty(value = "返回消息")
    private String message;
    
    @ApiModelProperty(value = "返回结果（单）")
    private T result;// 返回结果（单个）
}
