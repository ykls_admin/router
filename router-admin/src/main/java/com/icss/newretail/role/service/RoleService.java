package com.icss.newretail.role.service;

import com.icss.newretail.role.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author 
 * @since 2020-05-14
 */
public interface RoleService extends IService<Role> {

}
