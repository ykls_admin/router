package com.icss.newretail.role.dao;

import com.icss.newretail.role.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-05-14
 */
public interface RoleMapper extends BaseMapper<Role> {

}
