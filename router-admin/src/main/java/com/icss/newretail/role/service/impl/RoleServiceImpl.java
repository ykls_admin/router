package com.icss.newretail.role.service.impl;

import com.icss.newretail.role.entity.Role;
import com.icss.newretail.role.dao.RoleMapper;
import com.icss.newretail.role.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author 
 * @since 2020-05-14
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

}
