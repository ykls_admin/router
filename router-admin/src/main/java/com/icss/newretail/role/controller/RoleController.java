package com.icss.newretail.role.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.icss.newretail.role.entity.Role;
import com.icss.newretail.role.service.RoleService;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author 
 * @since 2020-05-14
 */
@Controller
@RequestMapping("/role")
public class RoleController {
  @Autowired
  private RoleService roleService;
  /**
   * 获取角色列表
   *
   * @return
   */
  @PostMapping("/getRoleListAll")
  @ResponseBody
  public List<Role> getRoleListAll() {
    return roleService.list();
  }
}
