package com.icss.newretail.user.dao;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.icss.newretail.user.entity.User;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-05-13
 */
public interface UserMapper extends BaseMapper<User> {
  IPage<User> getUserListByPage(IPage<User> page, @Param(Constants.WRAPPER) Wrapper<User> queryWrapper);
}
