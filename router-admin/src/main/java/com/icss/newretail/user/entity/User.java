package com.icss.newretail.user.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2020-05-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("task_job_user")
@ApiModel(value="User对象", description="")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "账号")
    @TableField("username")
    private String username;

    @ApiModelProperty(value = "密码")
    @TableField("password")
    private String password;

    @ApiModelProperty(value = "角色：0-普通用户、1-管理员")
    @TableField("role")
    private Integer role;

    @ApiModelProperty(value = "权限：执行器ID列表，多个逗号分割")
    @TableField("permission")
    private String permission;

    @ApiModelProperty(value = "角色Id")
    @TableField(exist = false)
    private String roleId;

    @ApiModelProperty(value = "角色编码")
    @TableField(exist = false)
    private String roleCode;

    @ApiModelProperty(value = "角色名称")
    @TableField(exist = false)
    private String roleName;
}
