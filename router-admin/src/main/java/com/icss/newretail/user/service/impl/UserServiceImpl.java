package com.icss.newretail.user.service.impl;

import java.time.LocalDateTime;
import java.util.UUID;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.icss.newretail.user.dao.UserMapper;
import com.icss.newretail.user.entity.User;
import com.icss.newretail.user.service.UserService;
import com.icss.newretail.userRole.dao.UserRoleMapper;
import com.icss.newretail.userRole.entity.UserRole;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2020-05-13
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserRoleMapper userRoleMapper;
    /**
     * 获取人员列表
     *
     * @return
     */
    @Override
    public IPage<User> getUserListByPage(IPage<User> page, @Param(Constants.WRAPPER) Wrapper<User> queryWrapper){
        return userMapper.getUserListByPage(page, queryWrapper);
    }
    @Override
    @Transactional()
    public void insertUser(User user) {
        userMapper.insert(user);
        UserRole userRole = new UserRole();
        userRole.setId(UUID.randomUUID().toString());
        userRole.setRoleId(user.getRoleId());
        userRole.setUserId(String.valueOf(user.getId()));
        userRole.setCreateUser(SecurityContextHolder.getContext().getAuthentication().getName());
        userRole.setCreateTime(LocalDateTime.now());
        userRoleMapper.delete(new QueryWrapper<UserRole>().eq("user_id", user.getId()));
        userRoleMapper.insert(userRole);
    }
    @Override
    @Transactional()
    public void updateUser(User user) {
        userMapper.updateById(user);
        UserRole userRole = new UserRole();
        userRole.setId(UUID.randomUUID().toString());
        userRole.setRoleId(user.getRoleId());
        userRole.setUserId(String.valueOf(user.getId()));
        userRole.setCreateUser(SecurityContextHolder.getContext().getAuthentication().getName());
        userRole.setCreateTime(LocalDateTime.now());
        userRoleMapper.delete(new QueryWrapper<UserRole>().eq("user_id", user.getId()));
        userRoleMapper.insert(userRole);
    }
    @Override
    @Transactional()
    public void deleteUser(String id) {
        userMapper.deleteById(id);
        userRoleMapper.delete(new QueryWrapper<UserRole>().eq("user_id", id));
    }
}
