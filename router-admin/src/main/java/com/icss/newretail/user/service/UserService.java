package com.icss.newretail.user.service;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.service.IService;
import com.icss.newretail.user.entity.User;
import com.icss.newretail.userRole.entity.UserRole;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2020-05-13
 */
public interface UserService extends IService<User> {
    /**
     * 获取任务列表
     *
     * @return
     */
    IPage<User> getUserListByPage(IPage<User> page, @Param(Constants.WRAPPER) Wrapper<User> queryWrapper);
    void insertUser(User user);
    void updateUser(User user);
    void deleteUser(String id);
}
