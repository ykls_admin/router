package com.icss.newretail.user.controller;


import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.icss.newretail.base.vo.ResponseBase;
import com.icss.newretail.base.vo.ResponseLayui;
import com.icss.newretail.user.entity.User;
import com.icss.newretail.user.service.UserService;
import com.icss.newretail.userRole.entity.UserRole;
import com.icss.newretail.userRole.service.UserRoleService;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2020-05-13
 */
@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private UserRoleService userRoleService;
    /**
     * 人员列表页面
     *
     * @return
     */
    @GetMapping("/userList")
    public String userList() {
        return "user/userList";
    }

    /**
     * 人员列表
     *
     * @return
     */
    @PostMapping("/getUserList")
    @ResponseBody
    public ResponseLayui<User> getUserList(Integer page, Integer limit,
                                           @RequestParam(value = "usernameQuery") String usernameQuery) {
        IPage<User> userPage = userService.getUserListByPage(new Page<>(page, limit),
                new QueryWrapper<User>()
                        .like(!StringUtils.isEmpty(usernameQuery),"a.username", usernameQuery)
                        .orderByAsc("a.username"));
        return new ResponseLayui<User>().setCode(0).setMsg("").setData(userPage.getRecords()).setCount(userPage.getTotal());
    }

    /**
     * 添加人员页面
     *
     * @return
     */
    @GetMapping("/userAdd")
    public String userAdd() {
        return "user/userAdd";
    }
    /**
     * 修改人员页面
     * @param model
     * @param id
     * @return
     */
    @GetMapping(value = "userEdit")
    public String userEdit(Model model, @RequestParam(value = "id") String id) {
        User user = userService.getById(id);
        UserRole userRole = userRoleService.getOne(new QueryWrapper<UserRole>().eq("user_id", user.getId()));
        user.setRoleId(userRole.getRoleId());
        model.addAttribute("user", user);
        return "user/userEdit";
    }
    /**
     * 添加人员
     *
     * @return
     */
    @PostMapping(value = "addUser")
    @ResponseBody
    public ResponseBase addUser(@RequestBody User user) {
        User userCheck = userService.getOne(new QueryWrapper<User>().eq("username", user.getUsername()));
        if (userCheck != null) {
            return new ResponseBase(500, "账号重复");
        }
        user.setPassword(DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));
        user.setRole(1);
        //保存数据库
        userService.insertUser(user);
        return new ResponseBase(200, "保存成功");
    }

    /**
     * 修改人员
     * @param user
     * @return
     */
    @PostMapping(value = "updateUser")
    @ResponseBody
    public ResponseBase updateUser(@RequestBody User user) {
        if (StringUtils.isEmpty(user.getPassword())) {
            user.setPassword(null);
        } else {
            user.setPassword(DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));
        }
        //保存数据库
        userService.updateUser(user);
        return new ResponseBase(200, "保存成功");
    }
    /**
     * 删除人员
     * @param id
     * @return
     */
    @PostMapping(value = "deleteUser")
    @ResponseBody
    public ResponseBase deleteUser(@RequestParam(value = "id") String id) {
        userService.deleteUser(id);
        return new ResponseBase(200, "删除成功");
    }
    /**
     * 修改密码
     * @param id
     * @return
     */
    @PostMapping(value = "updatePassword")
    @ResponseBody
    public ResponseBase updatePassword(@RequestParam(value = "id", required = false) Integer id,
        @RequestParam(value = "password") String password) {
        if (StringUtils.isEmpty(id)){
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            User user = userService.getOne(new QueryWrapper<User>().eq("username", username));
            id = user.getId();
        }
        User user = new User();
        user.setId(Integer.valueOf(id));
        user.setPassword(DigestUtils.md5DigestAsHex(password.getBytes()));
        userService.updateById(user);
        return new ResponseBase(200, "修改密码成功");
    }
}
