package com.icss.newretail.userRole.dao;

import com.icss.newretail.userRole.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户角色关系表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-05-14
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
