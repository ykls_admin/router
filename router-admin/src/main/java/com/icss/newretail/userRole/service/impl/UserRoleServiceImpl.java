package com.icss.newretail.userRole.service.impl;

import com.icss.newretail.userRole.entity.UserRole;
import com.icss.newretail.userRole.dao.UserRoleMapper;
import com.icss.newretail.userRole.service.UserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色关系表 服务实现类
 * </p>
 *
 * @author 
 * @since 2020-05-14
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

}
