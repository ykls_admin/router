package com.icss.newretail.userRole.service;

import com.icss.newretail.userRole.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色关系表 服务类
 * </p>
 *
 * @author 
 * @since 2020-05-14
 */
public interface UserRoleService extends IService<UserRole> {

}
