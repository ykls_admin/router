package com.icss.newretail.login.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.icss.newretail.util.JwtTokenUtil;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class LoginController {
    /**
     * 登录页面
     * @return
     */
    @GetMapping("/login_page")
    private String login_page() {
        return "login";
    }
    /**
     * 登录成功页面
     * @return
     */
    @GetMapping("/index")
    private String index(Model model) {
        String token = JwtTokenUtil.generateToken(SecurityContextHolder.getContext().getAuthentication().getName());
        model.addAttribute("token", token);
        return "index";
    }
    /**
     * 根路径
     * @return
     */
    @GetMapping("/")
    public String root() {
        return "redirect:/index";
    }
}
