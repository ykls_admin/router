package com.icss.newretail;

import java.util.concurrent.locks.AbstractQueuedSynchronizer;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
//@EnableWebMvc
@MapperScan("com.icss.newretail.*.dao")
public class RouterAdminApplication {
    public static void main(String[] args) {
//        SpringApplication.run(RouterAdminApplication.class, args);
        Object objA = new Object();
        Object objB = new Object();
        new Test(objA, objB).start();
        new Test(objB, objA).start();
    }
}
class Test extends Thread {
    private Object objA = null;
    private Object objB = null;
    public Test(Object objA, Object objB){
        this.objA = objA;
        this.objB = objB;
    }
    public void run(){
        synchronized (objA) {
            synchronized (objB) {
                System.out.println(1111);
            }
        }
    }
}

